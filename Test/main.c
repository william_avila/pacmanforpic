

#include <plib.h>
#include "MMB.h"
#include "Graphics/Graphics.h"
#include "LCDTerminal.h"
#include "MDD File System/FSIO.h"
#include "SerialFLASH.h"
#include "pacSprites.h"

#define GAME_NOT_STARTED 0
#define GAME_PLAYING 1
#define GAME_FINISHED 2
#define USE_PICTURE

// Configuration bits 
#pragma config POSCMOD = XT, FNOSC = PRIPLL, FSOSCEN = ON
#pragma config FPLLIDIV = DIV_2, FPLLMUL = MUL_20, FPLLODIV = DIV_1, FPBDIV = DIV_1
#pragma config OSCIOFNC = ON, CP = OFF, BWP = OFF, PWP = OFF

// fonts
extern const FONT_FLASH TerminalFont;

#define PutPacRight(x,y) PutImage(x,y,(void*)&pacRightSmall,IMAGE_NORMAL);
#define PutPacLeft(x,y) PutImage(x,y,(void*)&pacLeftSmall,IMAGE_NORMAL);
#define PutPacUp(x,y) PutImage(x,y,(void*)&pacUpSmall,IMAGE_NORMAL);
#define PutPacDown(x,y) PutImage(x,y,(void*)&pacDownSmall,IMAGE_NORMAL);

int main( void)
{
    int gameStarted = GAME_NOT_STARTED;

    typedef struct coordinates{
        int x;
        int y;
    } coord;

    coord ob1 = {2,5};
    coord ob2 = {16,3};
    coord ob3 = {5,19};
    coord ob4 = {10,20};
    coord ob5 = {6,8};
    coord obstacles[5] = {ob1, ob2, ob3, ob4, ob5};

    //    SearchRec rec;

    //-------------------------------------------------------------
    // initializations
	MMBInit();          // initialize the MikroE MMB board
        LCDInit();
    //-------------------------------------------------------------
    // Show Splash Screen
    SetColor( BRIGHTRED);
    LCDCenterString( -1, "MikroE PacByte Test");
    LCDCenterString(  0, "v 1.0");
    SetColor( WHITE);
    LCDCenterString(  2, "[Move Joystick to Start]");
    //MMBGetKey();
    MMBFadeIn( 250 );

    int posx = 0;
    int posy = 0;
    // main loop
    while( 1)
    {       
        if(gameStarted == GAME_NOT_STARTED){
            posx = 0;
            posy = 0;
            gameStarted = GAME_PLAYING;
        }
        else if(gameStarted == GAME_PLAYING){
            //Check the key currenty pressed
            int key = MMBReadKey();
            switch(key){
                case JOY_UP:
                    LCDClear();
                    if(posy >= pacSpriteHalfSize){
                        PutPacUp(posx,posy);
                        posy -= pacSpriteHalfSize;
                    }
                    break;
                case JOY_LEFT:
                    LCDClear();
                    if(posx >= pacSpriteHalfSize){
                        PutPacLeft(posx,posy);
                        posx -= pacSpriteHalfSize;
                    }
                    break;
                case JOY_RIGHT:
                    LCDClear();
                    if(posx <= GetMaxX()-pacSpriteHalfSize){
                        PutPacRight(posx,posy);
                        posx += pacSpriteHalfSize;
                    }
                    break;
                case JOY_DOWN:
                    LCDClear();
                    if(posy <= GetMaxY()-pacSpriteHalfSize){
                        PutPacDown(posx,posy);
                        posy += pacSpriteHalfSize;
                    }
                    break;
                case JOY_SELECT:
                    // Set the cursos to the top left corner
                    LCDClear();
                    posx = 0;
                    posy = 0;
                    PutPacRight(posx,posy);
                    break;
                default:
                    break;
            }
        }
        else if(gameStarted == GAME_FINISHED){
            gameStarted = GAME_NOT_STARTED;
        }
        // clear screen and start from the top
    } // main loop
} // main




